---
name: Amazon Web Services
country: usa
logo: assets/img/sponsors/aws.png
address: https://aws.amazon.com
---
