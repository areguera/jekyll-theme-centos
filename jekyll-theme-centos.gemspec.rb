# -*- coding: utf-8 -*-
################################################################################
#
# jekyll-theme-centos.gempsec.rb
#
# Script to create the "jekyll-theme-centos.gemspec" file. Required by the "gem
# build" command to produce the "jekyll-theme-centos.gem" file that is finally
# pushed up to rubygems registry. This script is expected to run inside GitLab
# CI/CD pipeline only.
#
# Copyright © 2022 Alain Reguera Delgado
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
require 'erb'

# Gem Author.
GITLAB_USER_NAME = ENV['GITLAB_USER_NAME']
GITLAB_USER_EMAIL = ENV['GITLAB_USER_EMAIL']

# Gem Name, Summary and Homepage.
CI_PROJECT_NAME = ENV['CI_PROJECT_NAME']
CI_PROJECT_DESCRIPTION = ENV['CI_PROJECT_DESCRIPTION']
CI_PROJECT_URL = ENV['CI_PROJECT_URL']

# Gem Version.
CI_COMMIT_TAG = ENV['CI_COMMIT_TAG'].delete("v").gsub("-beta", ".beta")

template = %q{# frozen_string_literal: true
Gem::Specification.new do |spec|
  spec.name     = "<%= CI_PROJECT_NAME %>"
  spec.version  = "<%= CI_COMMIT_TAG %>"
  spec.authors  = ["<%= GITLAB_USER_NAME %>"]
  spec.email    = ["<%= GITLAB_USER_EMAIL %>"]

  spec.summary  = "<%= CI_PROJECT_DESCRIPTION %>"
  spec.homepage = "<%= CI_PROJECT_URL %>"
  spec.license  = "MIT"

  spec.files    = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.required_ruby_version = "~> 3.0"
  spec.add_runtime_dependency "jekyll", "~> 4.2.0"
  spec.add_runtime_dependency "webrick", "~> 1.7.0"
  spec.add_development_dependency "bundler", "~> 2.3.0"
end
}

message = ERB.new(template, trim_mode: "%<>")

File.write("src/jekyll-theme-centos.gemspec", message.result)
